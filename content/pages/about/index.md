---
title: "About & contact"
openView: true
date: 2019-11-06
---

Antoine Jaunard (1990), designer from Brussels (BE), currently living in Amsterdam (NL).
Co-founder of a small but good web studio called [Variable](http://www.variable.club/).

- [write me some kind words](mailto:antoine.stuff@pm.me) on my encrypted email address
- [follow me](https://merveilles.town/@focus404) on an alternative social network
- [watch fragments of my life](https://www.instagram.com/focus.404/) on a website that I should stop using

{{< img name="antoine-chill.jpg" >}}

This website is made with a static site generator called [Hugo](https://gohugo.io/). The typeface in use is [Gap Sans](https://github.com/Interstices-/GapSans). All the sources are available on [Gitlab](https://gitlab.com/antoine.j/antoine).
