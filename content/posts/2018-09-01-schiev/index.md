---
title: "Schiev Festival"
categories: ["websites"]
tags: ["Variable"]
---

Design and front-end development of the Schiev Festival website, "a simple music festival" featuring concerts, talks & a label market.

{{< device type="desktop" name="variable-schiev-1.jpeg" >}}
{{< device type="desktop" name="variable-schiev-2.jpeg" >}}
{{< device type="mobile" name="variable-schiev-4.jpeg" >}}
{{< device type="mobile" name="variable-schiev-5.jpeg" >}}
{{< device type="desktop" name="variable-schiev-3.jpeg" >}}

{{< credit-variable >}}
